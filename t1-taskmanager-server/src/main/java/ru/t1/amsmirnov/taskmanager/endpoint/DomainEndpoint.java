package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IDomainEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.data.*;
import ru.t1.amsmirnov.taskmanager.dto.response.data.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataBase64();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBase64LoadResponse(e);
        }
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataBase64();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBase64SaveResponse(e);
        }
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinLoadResponse loadDataBin(@NotNull DataBinLoadRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataBin();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBinLoadResponse(e);
        }
        return new DataBinLoadResponse();
    }

    @NotNull
    @Override
    public DataBinSaveResponse saveDataBin(@NotNull DataBinSaveRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataBin();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBinSaveResponse(e);
        }
        return new DataBinSaveResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXMLResponse loadDataJsonFasterXML(@NotNull DataJsonLoadFasterXMLRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataJsonFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonLoadFasterXMLResponse(e);
        }
        return new DataJsonLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXMLResponse saveDataJsonFasterXML(@NotNull DataJsonSaveFasterXMLRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataJsonFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonSaveFasterXMLResponse(e);
        }
        return new DataJsonSaveFasterXMLResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadJaxbResponse loadDataJsonJaxB(@NotNull DataJsonLoadJaxbRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataJsonJaxB();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonLoadJaxbResponse(e);
        }
        return new DataJsonLoadJaxbResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveJaxbResponse saveDataJsonJaxB(@NotNull DataJsonSaveJaxbRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataJsonJaxB();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonSaveJaxbResponse(e);
        }
        return new DataJsonSaveJaxbResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXMLResponse loadDataXmlFasterXML(@NotNull DataXmlLoadFasterXMLRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataXMLFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlLoadFasterXMLResponse(e);
        }
        return new DataXmlLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXMLResponse saveDataXmlFasterXML(@NotNull DataXmlSaveFasterXMLRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataXMLFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlSaveFasterXMLResponse(e);
        }
        return new DataXmlSaveFasterXMLResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull DataXmlLoadJaxBRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataXMLJaxB();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlLoadJaxBResponse(e);
        }
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull DataXmlSaveJaxBRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataXMLJaxB();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlSaveJaxBResponse(e);
        }
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXMLResponse loadDataYAMLFasterXML(@NotNull DataYamlLoadFasterXMLRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataYAMLFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataYamlLoadFasterXMLResponse(e);
        }
        return new DataYamlLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXMLResponse saveDataYAMLFasterXML(@NotNull DataYamlSaveFasterXMLRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataYAMLFasterXML();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataYamlSaveFasterXMLResponse(e);
        }
        return new DataYamlSaveFasterXMLResponse();

    }
}
