package ru.t1.amsmirnov.taskmanager.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.*;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.api.service.*;
import ru.t1.amsmirnov.taskmanager.dto.request.data.*;
import ru.t1.amsmirnov.taskmanager.dto.request.project.*;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerVersionRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerAboutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.task.*;
import ru.t1.amsmirnov.taskmanager.dto.request.user.*;
import ru.t1.amsmirnov.taskmanager.endpoint.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.model.User;
import ru.t1.amsmirnov.taskmanager.repository.ProjectRepository;
import ru.t1.amsmirnov.taskmanager.repository.TaskRepository;
import ru.t1.amsmirnov.taskmanager.repository.UserRepository;
import ru.t1.amsmirnov.taskmanager.service.*;
import ru.t1.amsmirnov.taskmanager.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, projectService, taskService, propertyService);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    @Getter
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinLoadRequest.class, domainEndpoint::loadDataBin);
        server.registry(DataBinSaveRequest.class, domainEndpoint::saveDataBin);
        server.registry(DataJsonLoadFasterXMLRequest.class, domainEndpoint::loadDataJsonFasterXML);
        server.registry(DataJsonSaveFasterXMLRequest.class, domainEndpoint::saveDataJsonFasterXML);
        server.registry(DataJsonLoadJaxbRequest.class, domainEndpoint::loadDataJsonJaxB);
        server.registry(DataJsonSaveJaxbRequest.class, domainEndpoint::saveDataJsonJaxB);
        server.registry(DataXmlLoadFasterXMLRequest.class, domainEndpoint::loadDataXmlFasterXML);
        server.registry(DataXmlSaveFasterXMLRequest.class, domainEndpoint::saveDataXmlFasterXML);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::loadDataXmlJaxB);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::saveDataXmlJaxB);
        server.registry(DataYamlLoadFasterXMLRequest.class, domainEndpoint::loadDataYAMLFasterXML);
        server.registry(DataYamlSaveFasterXMLRequest.class, domainEndpoint::saveDataYAMLFasterXML);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::removeAll);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::create);
        server.registry(ProjectListRequest.class, projectEndpoint::findOneById);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeOneById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeOneByIndex);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::findOneById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::findOneByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateByIndex);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::removeAll);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::create);
        server.registry(TaskListRequest.class, taskEndpoint::findAll);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeOneById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeOneByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::findOneById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::findOneByIndex);
        server.registry(TaskShowByProjectIdRequest.class, taskEndpoint::findAllByProjectId);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateByIndex);
        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindToProject);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindFromProject);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changePassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUserByLogin);
        server.registry(UserRegistryRequest.class, userEndpoint::registry);
        server.registry(UserRemoveRequest.class, userEndpoint::removeByLogin);
        server.registry(UserRemoveByEmailRequest.class, userEndpoint::removeByEmail);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUserByLogin);
        server.registry(UserProfileRequest.class, userEndpoint::viewProfile);
        server.registry(UserUpdateRequest.class, userEndpoint::updateUserById);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        @NotNull final String fileName = "taskmanager(" + pid + ").pid";
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        try {
            @NotNull User admin = userService.create("admin", "admin", Role.ADMIN);
            @NotNull User test = userService.create("test", "test", "test@test.test");
            @NotNull User user = userService.create("user", "user");

            projectService.create(test.getId(), "Project 1", "Description 1");
            projectService.create(test.getId(), "Project 2", "Description 2");
            projectService.create(test.getId(), "Project 3", "Description 3");
            projectService.create(admin.getId(), "Admin project", "Description of the admin project");

            taskService.create(test.getId(), "TASK 1", "TASK 1");
            taskService.create(admin.getId(), "TASK 2", "TASK 2");
            taskService.create(user.getId(), "TASK 3", "TASK 3");

        } catch (@NotNull final Exception exception) {
            renderError(exception);
        }
    }

    private void renderError(@NotNull Exception exception) {
        loggerService.error(exception);
        System.out.println("[FAIL]");
    }

    public void start() {
        try {
            initPID();
            initDemoData();
            loggerService.info("** WELCOME TO TASK MANAGER **");
            Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
            backup.start();
            server.start();
        } catch (final Exception exception) {
            renderError(exception);
        }
    }

    public void stop() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        backup.stop();
        server.stop();
    }

}
