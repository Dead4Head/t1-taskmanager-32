package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IProjectEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.project.*;
import ru.t1.amsmirnov.taskmanager.dto.response.project.*;
import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Project;

import java.util.Comparator;
import java.util.List;

public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public ProjectChangeStatusByIdResponse changeStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        try {
            check(request);
            @Nullable final String projectId = request.getId();
            @Nullable final Status status = request.getStatus();
            @Nullable final String userId = request.getUserId();
            @NotNull final Project project = getServiceLocator().getProjectService().changeStatusById(userId, projectId, status);
            return new ProjectChangeStatusByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectChangeStatusByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        try {
            check(request);
            @Nullable final Integer index = request.getIndex();
            @Nullable final Status status = request.getStatus();
            @Nullable final String userId = request.getUserId();
            @NotNull final Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, status);
            return new ProjectChangeStatusByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectChangeStatusByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectClearResponse removeAll(@NotNull final ProjectClearRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            getServiceLocator().getProjectService().removeAll(userId);
            return new ProjectClearResponse();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectClearResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectCompleteByIdResponse completeById(@NotNull final ProjectCompleteByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().changeStatusById(userId, id, Status.COMPLETED);
            return new ProjectCompleteByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectCompleteByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectCompleteByIndexResponse completeByIndex(
            @NotNull final ProjectCompleteByIndexRequest request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, Status.COMPLETED);
            return new ProjectCompleteByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectCompleteByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectCreateResponse create(@NotNull final ProjectCreateRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull Project project = getServiceLocator().getProjectService().create(userId, name, description);
            return new ProjectCreateResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectCreateResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectListResponse findOneById(@NotNull final ProjectListRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final ProjectSort sort = request.getSort();
            Comparator<Project> comparator = null;
            if (sort != null)
                comparator = sort.getComparator();
            @NotNull List<Project> projects = getServiceLocator().getProjectService().findAll(userId, comparator);
            return new ProjectListResponse(projects);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectListResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectRemoveByIdResponse removeOneById(@NotNull final ProjectRemoveByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().removeOneById(userId, id);
            return new ProjectRemoveByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectRemoveByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectRemoveByIndexResponse removeOneByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().removeOneByIndex(userId, index);
            return new ProjectRemoveByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectRemoveByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectShowByIdResponse findOneById(@NotNull final ProjectShowByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().findOneById(userId, id);
            return new ProjectShowByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectShowByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectShowByIndexResponse findOneByIndex(@NotNull final ProjectShowByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().findOneByIndex(userId, index);
            return new ProjectShowByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectShowByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectStartByIdResponse startById(@NotNull final ProjectStartByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().changeStatusById(userId, id, Status.IN_PROGRESS);
            return new ProjectStartByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectStartByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectStartByIndexResponse startByIndex(@NotNull final ProjectStartByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
            return new ProjectStartByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectStartByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectUpdateByIdResponse updateById(@NotNull final ProjectUpdateByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull Project project = getServiceLocator().getProjectService().updateById(userId, id, name, description);
            return new ProjectUpdateByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectUpdateByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectUpdateByIndexResponse updateByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull Project project = getServiceLocator().getProjectService().updateByIndex(userId, index, name, description);
            return new ProjectUpdateByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectUpdateByIndexResponse(e);
        }
    }

}
