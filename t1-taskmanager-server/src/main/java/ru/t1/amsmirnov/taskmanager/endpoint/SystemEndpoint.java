package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.ISystemEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerAboutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerVersionRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerAboutResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerVersionResponse;

public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest serverAboutRequest) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse serverAboutResponse = new ServerAboutResponse();
        serverAboutResponse.setEmail(propertyService.getAuthorEmail());
        serverAboutResponse.setName(propertyService.getAuthorName());
        return serverAboutResponse;
    }

    @Override
    @NotNull
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest serverVersionRequest) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse serverVersionResponse = new ServerVersionResponse();
        serverVersionResponse.setVersion(propertyService.getApplicationVersion());
        return serverVersionResponse;
    }

}
