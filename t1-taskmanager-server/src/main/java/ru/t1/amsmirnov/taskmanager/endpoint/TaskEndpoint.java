package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.ITaskEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.task.*;
import ru.t1.amsmirnov.taskmanager.dto.response.task.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public TaskChangeStatusByIdResponse changeStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final Status status = request.getStatus();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, id, status);
            return new TaskChangeStatusByIdResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskChangeStatusByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @Nullable final Status status = request.getStatus();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, status);
            return new TaskChangeStatusByIndexResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskChangeStatusByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskClearResponse removeAll(@NotNull final TaskClearRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            getServiceLocator().getTaskService().removeAll(userId);
            return new TaskClearResponse();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskClearResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskCompleteByIdResponse completeById(@NotNull final TaskCompleteByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, id, Status.COMPLETED);
            return new TaskCompleteByIdResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCompleteByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskCompleteByIndexResponse completeByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, Status.COMPLETED);
            return new TaskCompleteByIndexResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCompleteByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskCreateResponse create(@NotNull final TaskCreateRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final Task task = getServiceLocator().getTaskService().create(userId, name, description);
            return new TaskCreateResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCreateResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskListResponse findAll(@NotNull final TaskListRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final TaskSort sort = request.getSort();
            Comparator<Task> comparator = null;
            if (sort != null)
                comparator = sort.getComparator();
            @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAll(userId, comparator);
            return new TaskListResponse(tasks);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskListResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskRemoveByIdResponse removeOneById(@NotNull final TaskRemoveByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().removeOneById(userId, id);
            return new TaskRemoveByIdResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskRemoveByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskRemoveByIndexResponse removeOneByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().removeOneByIndex(userId, index);
            return new TaskRemoveByIndexResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskRemoveByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskShowByIdResponse findOneById(@NotNull final TaskShowByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().findOneById(userId, id);
            return new TaskShowByIdResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskShowByIndexResponse findOneByIndex(@NotNull final TaskShowByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().findOneByIndex(userId, index);
            return new TaskShowByIndexResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskStartByIdResponse startById(@NotNull final TaskStartByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, id, Status.IN_PROGRESS);
            return new TaskStartByIdResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskStartByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskStartByIndexResponse startByIndex(@NotNull final TaskStartByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
            return new TaskStartByIndexResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskStartByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskUpdateByIdResponse updateById(@NotNull final TaskUpdateByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final Task task = getServiceLocator().getTaskService().updateById(userId, id, name, description);
            return new TaskUpdateByIdResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUpdateByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskUpdateByIndexResponse updateByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final Task task = getServiceLocator().getTaskService().updateByIndex(userId, index, name, description);
            return new TaskUpdateByIndexResponse(task);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUpdateByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskBindToProjectResponse bindToProject(@NotNull final TaskBindToProjectRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @Nullable final String taskId = request.getTaskId();
            getServiceLocator().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
            return new TaskBindToProjectResponse();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskBindToProjectResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskUnbindFromProjectResponse unbindFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @Nullable final String taskId = request.getTaskId();
            getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
            return new TaskUnbindFromProjectResponse();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUnbindFromProjectResponse(e);
        }
    }

    @Override
    @NotNull
    public TaskShowByProjectIdResponse findAllByProjectId(@NotNull TaskShowByProjectIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAllByProjectId(userId, projectId);
            return new TaskShowByProjectIdResponse(tasks);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByProjectIdResponse(e);
        }
    }

}
