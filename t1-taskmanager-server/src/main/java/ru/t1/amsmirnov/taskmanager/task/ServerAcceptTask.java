package ru.t1.amsmirnov.taskmanager.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.component.Server;

import java.net.ServerSocket;
import java.net.Socket;

public class ServerAcceptTask extends AbstractServerTask {

    public ServerAcceptTask(@NotNull final Server server) {
        super(server);
    }

    @Override
    public void run() {
        @Nullable ServerSocket serverSocket = server.getServerSocket();
        if (serverSocket == null) return;
        try {
            @NotNull final Socket socket = serverSocket.accept();
            server.submit(new ServerRequestTask(server, socket));
            server.submit(new ServerAcceptTask(server));
        } catch (final Exception exception) {
            server.getServiceLocator().getLoggerService().error(exception);
        }
    }

}
