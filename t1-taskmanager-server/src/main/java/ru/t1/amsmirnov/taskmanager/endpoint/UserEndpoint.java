package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IUserEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.user.*;
import ru.t1.amsmirnov.taskmanager.dto.response.user.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request) {
        try {
            check(request);
            @Nullable final String id = request.getUserId();
            @Nullable final String password = request.getPassword();
            @NotNull final User user = getServiceLocator().getUserService().setPassword(id, password);
            return new UserChangePasswordResponse(user);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserChangePasswordResponse(e);
        }
    }

    @Override
    @NotNull
    public UserLockResponse lockUserByLogin(@NotNull final UserLockRequest request) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            getServiceLocator().getUserService().lockUserByLogin(login);
            return new UserLockResponse();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserLockResponse(e);
        }
    }

    @Override
    @NotNull
    public UserRegistryResponse registry(@NotNull final UserRegistryRequest request) {
        try {
            @Nullable final String login = request.getLogin();
            @Nullable final String password = request.getPassword();
            @Nullable final String email = request.getEmail();
            @NotNull final User user = getServiceLocator().getUserService().create(login, password, email);
            return new UserRegistryResponse(user);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserRegistryResponse(e);
        }
    }

    @Override
    @NotNull
    public UserRemoveResponse removeByLogin(@NotNull final UserRemoveRequest request) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            @NotNull final User user = getServiceLocator().getUserService().removeByEmail(login);
            return new UserRemoveResponse(user);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserRemoveResponse(e);
        }
    }

    @Override
    @NotNull
    public UserRemoveByEmailResponse removeByEmail(@NotNull final UserRemoveByEmailRequest request) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String email = request.getEmail();
            @NotNull final User user = getServiceLocator().getUserService().removeByLogin(email);
            return new UserRemoveByEmailResponse(user);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserRemoveByEmailResponse(e);
        }
    }

    @Override
    @NotNull
    public UserUnlockResponse unlockUserByLogin(@NotNull final UserUnlockRequest request) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            getServiceLocator().getUserService().unlockUserByLogin(login);
            return new UserUnlockResponse();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserUnlockResponse(e);
        }
    }

    @Override
    @NotNull
    public UserProfileResponse viewProfile(@NotNull final UserProfileRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @NotNull final User user = getServiceLocator().getUserService().findOneById(userId);
            return new UserProfileResponse(user);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserProfileResponse(e);
        }
    }

    @Override
    @NotNull
    public UserUpdateResponse updateUserById(@NotNull UserUpdateRequest request) {
        try {
            check(request);
            @Nullable final String id = request.getUserId();
            @Nullable final String firstName = request.getFirstName();
            @Nullable final String lastName = request.getLastName();
            @Nullable final String middleName = request.getMiddleName();
            @NotNull final User user = getServiceLocator().getUserService().updateUserById(id, firstName, lastName, middleName);
            return new UserUpdateResponse(user);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserUpdateResponse(e);
        }
    }

}
