package ru.t1.amsmirnov.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IUserEndpointClient;
import ru.t1.amsmirnov.taskmanager.dto.request.user.*;
import ru.t1.amsmirnov.taskmanager.dto.response.user.*;
import ru.t1.amsmirnov.taskmanager.model.User;

public class UserEndpointClient extends AbstractEndpointClient implements IUserEndpointClient {

    public UserEndpointClient() {
    }

    public UserEndpointClient(@NotNull AuthEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    public UserLockResponse lockUserByLogin(@NotNull final UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    public UserRegistryResponse registry(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    @Override
    public UserRemoveResponse removeByLogin(@NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @Override
    public @NotNull UserRemoveByEmailResponse removeByEmail(@NotNull UserRemoveByEmailRequest request) {
        return call(request, UserRemoveByEmailResponse.class);
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUserByLogin(@NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    public UserProfileResponse viewProfile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @NotNull
    @Override
    public UserUpdateResponse updateUserById(@NotNull final UserUpdateRequest request) {
        return call(request, UserUpdateResponse.class);
    }

    public static void main(String[] args) {
        try {
            @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
            authEndpointClient.connect();
            authEndpointClient.login(new UserLoginRequest("test", "test"));
            @NotNull final UserEndpointClient userEndpointClient = new UserEndpointClient(authEndpointClient);
            @NotNull final UserRegistryResponse registryResponse = userEndpointClient.registry(new UserRegistryRequest("newUser","newUser","new@user"));
            System.out.println(registryResponse.isSuccess());
            @Nullable User user = registryResponse.getUser();
            if (user == null)
                System.out.println(registryResponse.getMessage());
            else
                System.out.println(registryResponse.getUser().getId());
            System.out.println();

            System.out.println(userEndpointClient.viewProfile(new UserProfileRequest()).getUser().getEmail());
            System.out.println();

            @NotNull UserLockResponse lockResponse = userEndpointClient.lockUserByLogin( new UserLockRequest("newUser"));
            System.out.println(lockResponse.isSuccess());
            System.out.println(lockResponse.getMessage());
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
