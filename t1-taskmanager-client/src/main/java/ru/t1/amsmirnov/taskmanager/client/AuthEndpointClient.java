package ru.t1.amsmirnov.taskmanager.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IAuthEndpointClient;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLoginRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLogoutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserProfileRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLoginResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLogoutResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserProfileResponse;

public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient() {
    }

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    public UserProfileResponse profile(@NotNull UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());

        System.out.println(
                authEndpointClient
                .login(new UserLoginRequest("admin", "admin1"))
                .isSuccess()
        );
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());

        System.out.println(
                authEndpointClient
                        .login(new UserLoginRequest("test", "test"))
                        .isSuccess()
        );
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
    }

}
