package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataJsonSaveFasterXMLRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataJsonSaveFasterXMLResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataJsonSaveFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json";

    @NotNull
    public static final String DESCRIPTION = "Save data to JSON file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveFasterXMLRequest request = new DataJsonSaveFasterXMLRequest();
        @NotNull final DataJsonSaveFasterXMLResponse response = getDomainEndpoint().saveDataJsonFasterXML(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
