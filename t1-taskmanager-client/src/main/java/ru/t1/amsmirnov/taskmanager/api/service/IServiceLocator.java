package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();


    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthEndpointClient getAuthEndpoint();

    @NotNull
    ISystemEndpointClient getSystemEndpoint();

    @NotNull
    IDomainEndpointClient getDomainEndpoint();

    @NotNull
    IProjectEndpointClient getProjectEndpoint();

    @NotNull
    ITaskEndpointClient getTaskEndpoint();

    @NotNull
    IUserEndpointClient getUserEndpoint();

}
