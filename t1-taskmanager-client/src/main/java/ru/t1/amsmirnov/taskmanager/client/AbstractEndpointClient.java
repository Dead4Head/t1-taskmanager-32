package ru.t1.amsmirnov.taskmanager.client;


import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IEndpointClient;

import java.io.*;
import java.net.Socket;

@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @Nullable
    private Socket socket;

    protected AbstractEndpointClient(@NotNull final String host, @NotNull final Integer port) {
        this.host = host;
        this.port = port;
    }

    protected AbstractEndpointClient(@NotNull final AuthEndpointClient client) {
        this.host = client.getHost();
        this.port = client.getPort();
        this.socket = client.getSocket();
    }

    @NotNull
    @SneakyThrows
    protected <T> T call(@NotNull final Object data, Class<T> T) {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        return (T) result;
    }


    @NotNull
    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    @NotNull
    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    public void connect() throws IOException {
        socket = new Socket(host, port);
    }

    public void disconnect() throws IOException {
        socket.close();
    }

    @Nullable
    @Override
    public Socket getSocket() {
        return socket;
    }

    @Override
    public void setSocket(@Nullable final Socket socket) {
        this.socket = socket;
    }

    @NotNull
    @Override
    public String getHost() {
        return host;
    }

    @Override
    public void setHost(@NotNull final String host) {
        this.host = host;
    }

    @NotNull
    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public void setPort(@NotNull final Integer port) {
        this.port = port;
    }

}
