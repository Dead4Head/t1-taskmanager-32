package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataJsonLoadFasterXMLRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataJsonLoadFasterXMLResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataJsonLoadFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json";

    @NotNull
    public static final String DESCRIPTION = "Load data from JSON file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataJsonLoadFasterXMLRequest request = new DataJsonLoadFasterXMLRequest();
        @NotNull final DataJsonLoadFasterXMLResponse response = getDomainEndpoint().loadDataJsonFasterXML(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
