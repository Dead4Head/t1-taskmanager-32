package ru.t1.amsmirnov.taskmanager.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IProjectEndpointClient;
import ru.t1.amsmirnov.taskmanager.dto.request.project.*;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLoginRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.project.*;
import ru.t1.amsmirnov.taskmanager.model.Project;

import java.util.List;

public class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpointClient {

    public ProjectEndpointClient() {
    }

    public ProjectEndpointClient(@NotNull AuthEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectClearResponse removeAll(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    public ProjectCompleteByIdResponse completeById(@NotNull final ProjectCompleteByIdRequest request) {
        return call(request, ProjectCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectCompleteByIndexResponse completeByIndex(@NotNull final ProjectCompleteByIndexRequest request) {
        return call(request, ProjectCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectCreateResponse create(@NotNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    public ProjectListResponse findOneById(@NotNull final ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeOneById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeOneByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectShowByIdResponse findOneById(@NotNull final ProjectShowByIdRequest request) {
        return call(request, ProjectShowByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectShowByIndexResponse findOneByIndex(@NotNull final ProjectShowByIndexRequest request) {
        return call(request, ProjectShowByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectStartByIdResponse startById(@NotNull final ProjectStartByIdRequest request) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectStartByIndexResponse startByIndex(@NotNull final ProjectStartByIndexRequest request) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateById(@NotNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse updateByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        authEndpointClient.login(new UserLoginRequest("test", "test"));
        @NotNull final ProjectEndpointClient projectEndpointClient = new ProjectEndpointClient(authEndpointClient);
        @Nullable List<Project> projectList = projectEndpointClient.findOneById(new ProjectListRequest()).getProjects();
        print(projectList);
        System.out.println();

        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest("ProjectEndpointCreated", "Description" );
        @NotNull final ProjectCreateResponse createResponse = projectEndpointClient.create(createRequest);
        @Nullable Project createdProject = createResponse.getProject();
        System.out.println(createdProject.getId());
        System.out.println(createdProject.getStatus());
        System.out.println();

        projectList = projectEndpointClient.findOneById(new ProjectListRequest()).getProjects();
        print(projectList);
        System.out.println();

        @NotNull final ProjectCompleteByIdResponse completeResponse = projectEndpointClient.completeById(new ProjectCompleteByIdRequest(createdProject.getId()));
        createdProject = completeResponse.getProject();
        System.out.println(completeResponse.isSuccess());
        System.out.println(createdProject.getStatus());
    }

    private static void print(@Nullable final List<Project> projectList) {
        if (projectList == null) {
            System.out.println("Projects not found");
            return;
        }
        for (final Project project : projectList) {
            System.out.println(project.getName() + " : " + project.getDescription());
        }
    }
}
