package ru.t1.amsmirnov.taskmanager.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.ITaskEndpointClient;
import ru.t1.amsmirnov.taskmanager.dto.request.task.*;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLoginRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.task.*;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpointClient {

    public TaskEndpointClient() {
    }

    public TaskEndpointClient(@NotNull AuthEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull TaskChangeStatusByIndexRequest request
    ) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskClearResponse removeAll(@NotNull TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeById(@NotNull TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse completeByIndex(@NotNull TaskCompleteByIndexRequest request) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskCreateResponse create(@NotNull TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    public TaskListResponse findAll(@NotNull TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeOneById(@NotNull TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeOneByIndex(@NotNull TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskShowByIdResponse findOneById(@NotNull TaskShowByIdRequest request) {
        return call(request, TaskShowByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskShowByIndexResponse findOneByIndex(@NotNull TaskShowByIndexRequest request) {
        return call(request, TaskShowByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskShowByProjectIdResponse findAllByProjectId(@NotNull TaskShowByProjectIdRequest request) {
        return call(request, TaskShowByProjectIdResponse.class);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startById(@NotNull TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse startByIndex(@NotNull TaskStartByIndexRequest request) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateById(@NotNull TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse updateByIndex(@NotNull TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindToProject(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        authEndpointClient.login(new UserLoginRequest("test", "test"));
        @NotNull final TaskEndpointClient taskEndpointClient = new TaskEndpointClient(authEndpointClient);
        @Nullable List<Task> taskList = taskEndpointClient.findAll(new TaskListRequest()).getTasks();
        print(taskList);
        System.out.println();

        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest("TaskEndpointCreated", "Description");
        @NotNull final TaskCreateResponse createResponse = taskEndpointClient.create(createRequest);
        @Nullable Task createdTask = createResponse.getTask();
        System.out.println(createdTask.getId());
        System.out.println(createdTask.getStatus());
        System.out.println();

        taskList = taskEndpointClient.findAll(new TaskListRequest()).getTasks();
        print(taskList);
        System.out.println();

        @NotNull final TaskCompleteByIdResponse completeResponse = taskEndpointClient.completeById(new TaskCompleteByIdRequest(createdTask.getId()));
        createdTask = completeResponse.getTask();
        System.out.println(completeResponse.isSuccess());
        System.out.println(createdTask.getStatus());
    }

    private static void print(@Nullable final List<Task> taskList) {
        if (taskList == null) {
            System.out.println("Task list empty");
            return;
        }
        for (final Task task : taskList) {
            System.out.println(task.getName() + " : " + task.getDescription());
        }
    }

}
