package ru.t1.amsmirnov.taskmanager.api.endpoint;

public interface IProjectEndpointClient extends IEndpointClient, IProjectEndpoint {

}
