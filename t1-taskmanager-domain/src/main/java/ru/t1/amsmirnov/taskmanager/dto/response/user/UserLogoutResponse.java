package ru.t1.amsmirnov.taskmanager.dto.response.user;

import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class UserLogoutResponse extends AbstractResultResponse {

}
