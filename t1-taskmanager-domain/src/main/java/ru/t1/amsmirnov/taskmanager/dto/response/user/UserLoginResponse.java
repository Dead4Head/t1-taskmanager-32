package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class UserLoginResponse extends AbstractResultResponse {

    public UserLoginResponse() {
    }

    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}