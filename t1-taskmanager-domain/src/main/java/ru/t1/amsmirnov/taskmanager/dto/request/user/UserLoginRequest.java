package ru.t1.amsmirnov.taskmanager.dto.request.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractRequest;

public final class UserLoginRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    public UserLoginRequest() {
    }

    public UserLoginRequest(@Nullable final String login, @Nullable final String password) {
        this.login = login;
        this.password = password;
    }

    @Nullable
    public String getLogin() {
        return login;
    }

    public void setLogin(@Nullable final String login) {
        this.login = login;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    public void setPassword(@Nullable final String password) {
        this.password = password;
    }

}