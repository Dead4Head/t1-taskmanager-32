package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;
import ru.t1.amsmirnov.taskmanager.model.Task;

public abstract class AbstractTaskResponse extends AbstractResultResponse {

    @Nullable
    private Task task;

    protected AbstractTaskResponse(@Nullable final Task task) {
        this.task = task;
    }

    protected AbstractTaskResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    @Nullable
    public Task getTask() {
        return task;
    }

}