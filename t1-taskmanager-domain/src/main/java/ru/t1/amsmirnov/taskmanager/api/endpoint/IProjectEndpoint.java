package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.project.*;
import ru.t1.amsmirnov.taskmanager.dto.response.project.*;

public interface IProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    ProjectChangeStatusByIndexResponse changeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    ProjectClearResponse removeAll(@NotNull ProjectClearRequest request);

    @NotNull
    ProjectCompleteByIdResponse completeById(@NotNull ProjectCompleteByIdRequest request);

    @NotNull
    ProjectCompleteByIndexResponse completeByIndex(@NotNull ProjectCompleteByIndexRequest request);

    @NotNull
    ProjectCreateResponse create(@NotNull ProjectCreateRequest request);

    @NotNull
    ProjectListResponse findOneById(@NotNull ProjectListRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeOneById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse removeOneByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectShowByIdResponse findOneById(@NotNull ProjectShowByIdRequest request);

    @NotNull
    ProjectShowByIndexResponse findOneByIndex(@NotNull ProjectShowByIndexRequest request);

    @NotNull
    ProjectStartByIdResponse startById(@NotNull ProjectStartByIdRequest request);

    @NotNull
    ProjectStartByIndexResponse startByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByIndexResponse updateByIndex(@NotNull ProjectUpdateByIndexRequest request);

}
