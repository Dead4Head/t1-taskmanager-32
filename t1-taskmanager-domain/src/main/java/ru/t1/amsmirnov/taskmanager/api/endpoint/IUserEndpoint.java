package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.user.*;
import ru.t1.amsmirnov.taskmanager.dto.response.user.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lockUserByLogin(@NotNull UserLockRequest request);

    @NotNull
    UserRegistryResponse registry(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse removeByLogin(@NotNull UserRemoveRequest request);

    @NotNull
    UserRemoveByEmailResponse removeByEmail(@NotNull UserRemoveByEmailRequest request);

    @NotNull
    UserUnlockResponse unlockUserByLogin(@NotNull UserUnlockRequest request);

    @NotNull
    UserProfileResponse viewProfile(@NotNull UserProfileRequest request);

    @NotNull
    UserUpdateResponse updateUserById(@NotNull UserUpdateRequest request);

}
