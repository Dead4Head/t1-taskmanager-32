package ru.t1.amsmirnov.taskmanager.dto.request.project;

import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class ProjectClearRequest extends AbstractUserRequest {
}
