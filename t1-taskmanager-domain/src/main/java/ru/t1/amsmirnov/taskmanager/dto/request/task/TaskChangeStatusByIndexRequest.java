package ru.t1.amsmirnov.taskmanager.dto.request.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;

public final class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public TaskChangeStatusByIndexRequest() {
    }

    public TaskChangeStatusByIndexRequest(
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        this.index = index;
        this.status = status;
    }

    @Nullable
    public Integer getIndex() {
        return index;
    }

    public void setIndex(@Nullable final Integer index) {
        this.index = index;
    }

    @Nullable
    public Status getStatus() {
        return status;
    }

    public void setStatus(@Nullable final Status status) {
        this.status = status;
    }

}