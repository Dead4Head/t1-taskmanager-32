package ru.t1.amsmirnov.taskmanager.dto.request.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class TaskShowByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskShowByIdRequest() {
    }

    public TaskShowByIdRequest(@Nullable final String id) {
        this.id = id;
    }

    @Nullable
    public String getId() {
        return id;
    }

    public void setId(@Nullable final String id) {
        this.id = id;
    }

}