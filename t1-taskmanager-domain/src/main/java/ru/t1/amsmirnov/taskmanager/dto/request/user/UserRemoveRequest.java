package ru.t1.amsmirnov.taskmanager.dto.request.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class UserRemoveRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserRemoveRequest() {
    }

    public UserRemoveRequest(@Nullable final String login) {
        this.login = login;
    }

    @Nullable
    public String getLogin() {
        return login;
    }

    public void setLogin(@Nullable final String login) {
        this.login = login;
    }

}