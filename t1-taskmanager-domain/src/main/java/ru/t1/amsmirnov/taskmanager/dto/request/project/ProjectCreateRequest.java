package ru.t1.amsmirnov.taskmanager.dto.request.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class ProjectCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectCreateRequest() {
    }

    public ProjectCreateRequest(
            @Nullable final String name,
            @Nullable final String description
    ) {
        this.name = name;
        this.description = description;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable final String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable final String description) {
        this.description = description;
    }

}
