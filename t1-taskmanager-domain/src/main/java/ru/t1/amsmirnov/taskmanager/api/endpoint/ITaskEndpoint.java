package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.task.*;
import ru.t1.amsmirnov.taskmanager.dto.response.task.*;

public interface ITaskEndpoint {

    @NotNull
    TaskChangeStatusByIdResponse changeStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse removeAll(@NotNull TaskClearRequest request);

    @NotNull
    TaskCompleteByIdResponse completeById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse completeByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    TaskCreateResponse create(@NotNull TaskCreateRequest request);

    @NotNull
    TaskListResponse findAll(@NotNull TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse removeOneById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeOneByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskShowByIdResponse findOneById(@NotNull TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse findOneByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull
    TaskShowByProjectIdResponse findAllByProjectId(@NotNull TaskShowByProjectIdRequest request);

    @NotNull
    TaskStartByIdResponse startById(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse startByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull
    TaskUpdateByIdResponse updateById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateByIndex(@NotNull TaskUpdateByIndexRequest request);

    @NotNull
    TaskBindToProjectResponse bindToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindFromProject(@NotNull TaskUnbindFromProjectRequest request);

}
