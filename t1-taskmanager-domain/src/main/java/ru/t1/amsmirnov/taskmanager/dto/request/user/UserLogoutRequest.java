package ru.t1.amsmirnov.taskmanager.dto.request.user;

import ru.t1.amsmirnov.taskmanager.dto.request.AbstractRequest;

public final class UserLogoutRequest extends AbstractRequest {
}
