package ru.t1.amsmirnov.taskmanager.dto.request.task;

import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class TaskClearRequest extends AbstractUserRequest {

    public TaskClearRequest() {
    }

}