package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.*;
import ru.t1.amsmirnov.taskmanager.dto.response.data.*;

public interface IDomainEndpoint {

    @NotNull
    DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinLoadResponse loadDataBin(@NotNull DataBinLoadRequest request);

    @NotNull
    DataBinSaveResponse saveDataBin(@NotNull DataBinSaveRequest request);

    @NotNull
    DataJsonLoadFasterXMLResponse loadDataJsonFasterXML(@NotNull DataJsonLoadFasterXMLRequest request);

    @NotNull
    DataJsonSaveFasterXMLResponse saveDataJsonFasterXML(@NotNull DataJsonSaveFasterXMLRequest request);

    @NotNull
    DataJsonLoadJaxbResponse loadDataJsonJaxB(@NotNull DataJsonLoadJaxbRequest request);

    @NotNull
    DataJsonSaveJaxbResponse saveDataJsonJaxB(@NotNull DataJsonSaveJaxbRequest request);

    @NotNull
    DataXmlLoadFasterXMLResponse loadDataXmlFasterXML(@NotNull DataXmlLoadFasterXMLRequest request);

    @NotNull
    DataXmlSaveFasterXMLResponse saveDataXmlFasterXML(@NotNull DataXmlSaveFasterXMLRequest request);

    @NotNull
    DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull DataXmlLoadJaxBRequest request);

    @NotNull
    DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull DataXmlSaveJaxBRequest request);

    @NotNull
    DataYamlLoadFasterXMLResponse loadDataYAMLFasterXML(@NotNull DataYamlLoadFasterXMLRequest request);

    @NotNull
    DataYamlSaveFasterXMLResponse saveDataYAMLFasterXML(@NotNull DataYamlSaveFasterXMLRequest request);

}
