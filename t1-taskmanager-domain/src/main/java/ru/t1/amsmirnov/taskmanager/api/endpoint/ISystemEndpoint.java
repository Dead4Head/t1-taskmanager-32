package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerAboutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerVersionRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerAboutResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest serverAboutRequest);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest serverVersionRequest);

}
