package ru.t1.amsmirnov.taskmanager.dto.response.server;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResponse;

public final class ServerVersionResponse extends AbstractResponse {

    @Nullable
    private String version;

    @Nullable
    public String getVersion() {
        return this.version;
    }

    public void setVersion(@Nullable final String version) {
        this.version = version;
    }

}
