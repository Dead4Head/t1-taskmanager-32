package ru.t1.amsmirnov.taskmanager.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Project;

public final class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectShowByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}